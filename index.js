// load the express module into our application and save it in a variable called express.
const express = require("express");

// localhost port number
const port = 4000;

//**to create a server using express, contain it to a variable app
// app is our server
// create an application that uses and stores it as app.
const app = express();

// middleware
//**app has a method "use", and express has a method "json", to convert automatically
// express.json() is a method which allows us to handle the streaming of data and automatically parse the incoming json from our request.
app.use(express.json());

//mockdata
let users = [
		{
			username: "TStark3000",
			email: "starkindustries@mail.com",
			password: "notPeterParker"
		},
		{
			username: "ThorThunder",
			email: "loveandthunder@mail.com",
			password: "iLoveStromBreaker"
		}
	]

	// express has methods to use as routes corresponding to HTTP methods
	// SYNTAX:    
		// app.method(<endpoint>, function for request and response)

	// [HTTP method GET]
		app.get("/", (request, response) => {

			response.send("Hello from express!")
			// response.status = writeHead
			// response.send = write with end()
			// response.status(201).send("Hello from express!")
		})

	// MINI ACTIVITY
		//endpoint: /greeting
		//message: "Hello from Batch245-surname"

		app.get("/greeting", (request, response) => {
			response.status(201).send("Hello from Batch245-SanRoque!!")
		})
	// End MiniActivity

	app.get("/users", (request, response) => {
			response.send(users)
		})

	// [HTTP method for POST]
		app.post("/users", (request, response) => {
			let input = request.body;

			
			let newUser = {
				username: input.username,
				email: input.email,
				password: input.password
			}

			users.push(newUser);

			response.send(`The ${newUser.username} is now registered in our website with email: ${newUser.email}!`);
		})

	// [HTTP method for DELETE]
		app.delete("/users", (request, response) => {

			let deletedUser = users.pop();

			response.send(deletedUser);
		})

	// [HTTP method for PUT]
		app.put("/users/:index", (request, response) => {

			let indexToBeUpdated = request.params.index
				//**params - not changing, a property

			console.log(typeof indexToBeUpdated); //string
			indexToBeUpdated = parseInt(indexToBeUpdated); //converted to num (Int)
			
			
			if(indexToBeUpdated < users.length){
				users[indexToBeUpdated].password = request.body.password;
			}

			response.send(users[indexToBeUpdated]);
		})


app.listen(port, ()=>console.log("server is running at port 4000"));

